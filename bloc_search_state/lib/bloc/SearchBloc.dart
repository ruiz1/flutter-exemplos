import 'package:github_search/models/SearchResult.dart';
import 'package:github_search/services/data/githubService.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';

class SearchBloc {
  
  String nome = 'ruiz';

  GitHubeService _service = new GitHubeService();

  final _searchcontroller = new BehaviorSubject<String>();
  
  Sink<String> get searchEvent => _searchcontroller.sink;
  Observable<String> get searchFlux => _searchcontroller.stream;

  Observable<SearchResult> apiResultFlux;

  SearchBloc(){
    print(nome);
    apiResultFlux = searchFlux
    .distinct()
    .where((valor) => valor.length>2)
    .asyncMap(_service.search) 
    .switchMap((valor) => Observable.just(valor));

     // inicializa com valor padrao
     searchEvent.add(nome);

  }

  void dispose(){
    _searchcontroller.close();
  }

}