<?php

use App\Models\User;
use App\Models\Team;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'Adriano Ruiz',
            'email' => 'ruiz@7cliques.com.br',
            'password' => Hash::make('cli007'),
            'role' => 'admin',
            'avatar' => '/assets/img/avatar/emanuel.jpg',
        ]);
        User::create([
            'name' => 'Moacyr',
            'email' => 'desenvolvimento@7cliques.com.br',
            'password' => Hash::make('cli007'),
            'role' => 'admin',
            'avatar' => 'https: //pbs.twimg.com/profile_images/974736784906248192/gPZwCbdS.jpg',
        ]);

        User::create([
            'name' => 'Gabriel Scaranello',
            'email' => 'desenvolvimento2@7cliques.com.br',
            'password' => Hash::make('@S3v3n'),
            'role' => 'admin',
            'avatar' => 'https://semanadecursos2019.ifpreventos.com.br/uploads/avatar/A-1555177442.jpg',
        ]);

        User::create([
            'name' => 'Eduardo Bail',
            'email' => 'desenvolvimento3@7cliques.com.br',
            'password' => Hash::make('cli007'),
            'role' => 'admin',
            'avatar' => 'https: //pbs.twimg.com/profile_images/974736784906248192/gPZwCbdS.jpg',
        ]);
        User::create([
            'name' => 'Juliana',
            'email' => 'dev4@7cliques.com.br',
            'password' => Hash::make('cli007'),
            'role' => 'admin',
            'avatar' => 'https: //pbs.twimg.com/profile_images/974736784906248192/gPZwCbdS.jpg',
        ]);

        User::create([
            'name' => 'Fernando',
            'email' => 'dev5@7cliques.com.br',
            'password' => Hash::make('cli007'),
            'role' => 'admin',
            'avatar' => 'https: //pbs.twimg.com/profile_images/974736784906248192/gPZwCbdS.jpg',
        ]);

        User::create([
            'name' => 'Cléo',
            'email' => 'financeiro@7cliques.com.br',
            'password' => Hash::make('cli007'),
            'role' => 'admin',
            'avatar' => 'https: //pbs.twimg.com/profile_images/974736784906248192/gPZwCbdS.jpg',
        ]);

        Team::create(['team' => 'Full Stack',]);
        Team::create(['team' => 'Front And']);
        Team::create(['team' => 'Tester and Customer Success']);
        Team::create(['team' => 'Suporte']);
        Team::create(['team' => 'Product Design, UI and UX']);
        Team::create(['team' => 'CEO Adminsitrativo']);


    }
}
