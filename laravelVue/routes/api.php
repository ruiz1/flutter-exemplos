<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// rotas personalizadas
Route::get('scores', 'Api\TaskscoreController@timeScore');


// rota para limpar o Cache
Route::delete('cache', function () {
    if (\Cache::flush()) {
        return response()->json([], 200);
    }
    return response()->json([], 500);
});

// Rotas de autenticação
Route::group(['middleware' => ['api'], 'prefix' => 'auth'], function ($router) {
    Route::post('login', 'Api\AuthController@login')->name('login');
    Route::post('logout', 'Api\AuthController@logout');
    Route::post('forgot', 'Api\AuthController@forgot');
    Route::post('refresh', 'Api\AuthController@refresh');
    Route::post('me', 'Api\AuthController@me');
    Route::post('check', 'Api\AuthController@check');
});

// //Rotas autenticadas
Route::group(['prefix' => '/', 'namespace' => 'Api\\', 'middleware' => ['auth:api']], function ($router) {

   



    //rotas RESOURCES
    Route::resources([
        'taskscore' => 'TaskscoreController',
        'ponto' => 'PontoController',
        'user' => 'UserController',
        'team' => 'TeamController',

    ]);




});

//Rotas Sem autenticaçao
Route::group(['prefix' => '/', 'namespace' => 'Api\\', 'middleware' => ['api']], function ($router) {

    //rotas personalizadas

    // rotas resources
    Route::resources([]);
});


