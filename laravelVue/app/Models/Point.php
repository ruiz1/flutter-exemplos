<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $fillable = [
        'user_id', 'team_id','total_tasks', 'finished_tasks', 'score', 'status'
    ];

    public function User(){
        return $this->belongsTo(User::class);
    }
     public function Team(){
        return $this->belongsTo(Team::class);
    }
}

