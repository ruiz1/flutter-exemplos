<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Point;
use App\Models\User;
use Illuminate\Http\Request;

class TaskscoreController extends Controller
{

    public function index()
    {
        $pontos = Point::with('User', 'Team')->get();
        if ($pontos) {
            return $pontos; //['status'=> 'success', 'pontos' => $pontos];

        }

        return ['status' => 'error'];

    }

    public function userScore($id)
    {
        $pontos = Point::with('User', 'Team')
            ->where('user_id', $id)->get();

        if ($pontos) {
            $total_times = $pontos->count();
            $sum_points = $pontos->sum('score');
            $score = 0;
            if ($sum_points > 0) {
                $score = $sum_points / $total_times;
            }
            return $score;

        }

        return ['status' => 'error'];

    }

    public function timeScore()
    {

        $users = User::where('id', '<>', 7)->orderby('name', 'ASC')->get();

        if ($users) {

            foreach ($users as $key => $value) {
                $score = $this->userScore($value->id);
                $status = $this->calcStatus($score, $value->id);

                // echo "<p>$value->name - $score - $status</p> ";
                $user[$key]['score'] = $score;
                $user[$key]['status'] = $status;
                $user[$key]['name'] = $value->name;

            }

            $collection = collect($user);
            $sorted = $collection->sortByDesc('score');
            return $sorted->values()->all();

        }

        // return ['status'=>'error'];

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $pontos = Point::create($data);
        $score = $this->calcScore($pontos->id, $request->total_tasks, $request->finished_tasks, $request->user_id);

        return 'success';
    }

    public function calcStatus($score, $user_id)
    {
        $user = User::where('id', $user_id)->first();
        if ($score < 50) {
            $status = 'Arqueiro Verde';
            if ($user->sexo == 'F') {
                $status = 'Nebulosa';
            }
        }
        if ($score >= 50) {
            $status = 'Capitão America';
            if($user->sexo =='F') {
                $status = 'Viúva Negra';
            }
        }
        if ($score >= 70) {
            $status = 'Hulk';
            if ($user->sexo == 'F') {
                $status = 'Nebulosa';
            }
            if ($user->sexo == 'F') {
                $status = 'Jean Grey ';
            }
        }
        if ($score >= 80) {
            $status = 'Thor';
            if ($user->sexo == 'F') {
                $status = 'Feiticeira Escarlate';
            }
        }
        if ($score >= 90) {
            $status = 'Sr. Stark';
            if ($user->sexo == 'F') {
                $status = 'Capita Marvel';
            }
        }

        return $status;

    }

    public function calcScore($id, $total_tasks, $finished_tasks, $user_id)
    {

        $score = ($finished_tasks * 100) / $total_tasks;
        $status = $this->calcStatus($score, $user_id);
        Point::where('id', $id)->update(['score' => $score, 'status' => $status]);

        return $score;

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $pontos = Point::select('points.*','users.name')
        //  ->JOIN('users', 'users.id', '=', 'points.user_id')
        //  ->where('points.id', $id)->first();
        $pontos = Point::find($id);
        if ($pontos) {
            return $pontos; //['status'=> 'success', 'pontos' => $pontos];

        }

        return ['status' => 'error'];

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        Point::where('id', $id)->update($data);
        $score = $this->calcScore($id, $request->total_tasks, $request->finished_tasks, $request->user_id);

        return 'success';

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

}
