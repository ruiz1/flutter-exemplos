const routes = [{
    path: '/',
    redirect: '/admin',
}, {
    path: '/login',
    name: 'Login',
    component: () => import('@/pages/login.vue'),
}, {
    path: '/admin',
    component: () => import('@/pages/app.vue'),
    children: [{
        path: '',
        name: 'Dashboard',
        component: () => import('@/pages/dashboard.vue'),
    }, {
        path: 'taskscores',
        name: 'taskscore',
        component: () => import('@/pages/taskscore/index.vue'),
    }, {
        path: 'taskscore',
        name: 'Cadastrar taskscore',
        component: () => import('@/pages/taskscore/form.vue'),
    }, {
        path: 'taskscore/:id',
        name: 'Editar taskscore',
        component: () => import('@/pages/taskscore/form.vue'),
    }, {
        path: 'pontos',
        name: 'ponto',
        component: () => import('@/pages/ponto/index.vue'),
    }, {
        path: 'ponto',
        name: 'Cadastrar ponto',
        component: () => import('@/pages/ponto/form.vue'),
    }, {
        path: 'ponto/:id',
        name: 'Editar ponto',
        component: () => import('@/pages/ponto/form.vue'),
        }, {
            path: 'scores',
            name: 'Score',
            component: () => import('@/pages/taskscore/score.vue'),
        },

],
}];

export default routes;
