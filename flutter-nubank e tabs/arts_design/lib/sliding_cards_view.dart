import 'package:flutter/material.dart';
import 'dart:math' as math;

class SlidingCardsView extends StatefulWidget {
  @override
  _SlidingCardsViewState createState() => _SlidingCardsViewState();
}

class _SlidingCardsViewState extends State<SlidingCardsView> {
  PageController pageController;
  double pageOffset = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pageController = PageController(viewportFraction: 0.8);
    pageController.addListener((){
      setState(() => pageOffset = pageController.page);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    pageController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.55,
      child: PageView(
        controller: pageController,
        children: <Widget>[
          
          SlidingCard(
            name: 'Shenzhen GLOBAL DESIGN AWARD 2018',
            date: '4.20-30',
            assetName: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToBA-aEi4xeNfdjGyDwAx6FG9LMItsYmrDvb-O177_n9seatc1',
            offset: pageOffset,
          ),
          SlidingCard(
            name: 'Shenzhen GLOBAL DESIGN AWARD 2018',
            date: '4.20-31',
            assetName: 'https://cpb-us-e1.wpmucdn.com/blogs.uoregon.edu/dist/e/7360/files/2014/05/michael-jackson-art-hd-wallpapers-231r1lx.jpg',
            offset: pageOffset - 1,
          )

        ],
      ),
    );
  }

}

class SlidingCard extends StatelessWidget {
  final String name;
  final String date;
  final String assetName;
  final double offset;
  
  const SlidingCard({
    Key key, 
    @required this.name, 
    @required this.date, 
    @required this.assetName, 
    @required this.offset,
    
  });

  @override
  Widget build(BuildContext context) {
    double gauss = math.exp(-(math.pow((offset.abs() - 0.5), 2) / 0.08));

    // TODO: implement build
    return Transform.translate(
      offset: Offset(-32 * gauss * offset.sign, 0),
      child: Card(
        margin: EdgeInsets.only(left: 8, right: 10, bottom: 24),
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
        child: Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.vertical(top: Radius.circular(32)),
              child: Image.network(assetName,
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.8,
              alignment: Alignment(-offset.abs(), 0),
              fit: BoxFit.fitWidth,

              ),
              
            ),

            SizedBox(height: 8,),
            Expanded(
              child: CardContent(
                name: name,
                date: date,
                offset: gauss,
              ),
            )
          ],
        ),
      ),
    );
  }
  
}

class CardContent extends StatelessWidget {
  final String name;
  final String date;
  final double offset;
  const CardContent(
    {Key key,
    @required this.name,
    @required this.date,
    @required this.offset})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Transform.translate(
            offset: Offset(8 * offset, 0),
            child: Text(name, style: TextStyle(fontSize: 20)),
          ),
          SizedBox(height: 8,),

          Transform.translate(
            offset: Offset(32 * offset, 0),
            child: Text(
              date,
              style: TextStyle(color: Colors.grey),
            ),
          ),

          Spacer(),
          Row(
            children: <Widget>[

              Transform.translate(
                offset: Offset(48 * offset, 0),
                child: RaisedButton(
                  color: Color(0xFF162A49),
                  child: Transform.translate(
                    offset: Offset(24 * offset, 0),
                    child: Text('Reserve'),
                  ),

                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(32),
                  ),
                  onPressed: () {},
                ),
              ),

              Spacer(),
              Transform.translate(
                offset: Offset(32 * offset, 0),
                child: Text(
                  '0,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
              SizedBox(width: 16,),



            ],
          )

        ],
      ),
      
    );
  }
}

