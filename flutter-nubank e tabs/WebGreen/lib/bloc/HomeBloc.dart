import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:web_pv/bloc/mixin/validator.dart';

class ValueBloc extends BlocBase with Validator{
  
    final _fullNameController = BehaviorSubject<String>();
    final _dataNascimento = BehaviorSubject<String>();
    final _identificacao = BehaviorSubject<String>();
    

    //adding data to stream with sink.add
    Function(String) get fullNameUpdate => _fullNameController.sink.add;
    StreamSink get dataNascUpdate => _dataNascimento.sink;
    Function(String) get identUpdate => _identificacao.sink.add;
    


     //Listening / retrieving data from stream with stream.listen
    Stream<String> get fullName => _fullNameController.stream.transform(validateFullName);
    Stream<String> get dataNasc => _dataNascimento.stream.transform(validateDataNasc);
    Stream<String> get identificacao => _identificacao.stream.transform(validateIdentidade);
    Stream<bool> get submitCheck => Observable.combineLatest2(fullName, identificacao, (a,b) => true);

    submitButtonPressed(){
      
      final validFullName = _fullNameController.value;
      final validNascimento = _dataNascimento.value;
      final validIdentificacao = _identificacao.value;
      
    }


    @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _fullNameController.close();
    _dataNascimento.close();
    _identificacao.close();
  }
}



