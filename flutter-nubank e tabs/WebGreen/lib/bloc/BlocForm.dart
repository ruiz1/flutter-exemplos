import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';

class BlocForm extends BlocBase{
    
    int _pageIndex = 0;
     //Pagecontroller
    StreamController<int> __nextPageController = StreamController<int>();
    StreamSink<int> get nextPageAdd => __nextPageController.sink;
    Stream<int> get nextPageOut => __nextPageController.stream;

    /************************************************************/
  
}