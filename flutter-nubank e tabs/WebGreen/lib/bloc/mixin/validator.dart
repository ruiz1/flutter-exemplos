import 'dart:async';

class Validator {


  final validateFullName = StreamTransformer<String, String>.fromHandlers(
    handleData: (fullName, sink){
       if(fullName.length > 5 ){
          sink.add(fullName);
        }else {
          sink.addError('Nome não pode ficar em branco');
        }
    }
  );


   final validateDataNasc = StreamTransformer<String, String>.fromHandlers(
    handleData: (dataNasc, sink){
       RegExp validarDataNasc = new RegExp(r"\d{2}\d{2}");
       bool isvalid;
      if(dataNasc.isEmpty){
            sink.addError('Data não pode ficar vazio');
      }else {
       if(dataNasc.length == 10){
         
          List<String> listaData = dataNasc.split('/');
          int dia = int.parse(listaData[0]);
          int mes = int.parse(listaData[1]);
          int ano = int.parse(listaData[2]);

          if( 
              dia <= 31 &&  
              mes <= 12 &&  
              ano <= 2019 && ano.toString().length == 4 )

            {

            sink.add(dataNasc);

          }else{
             sink.addError('Data inválida');
          }

         
       }
      }
    }

  );

  final validateIdentidade = StreamTransformer<String, String>.fromHandlers(
    handleData: (identidade, sink){
        RegExp validarIdentidade = new RegExp(r"\d{9}");
      if(validarIdentidade.hasMatch(identidade)){
        sink.add(identidade);
      }else{
        sink.addError('Identidade inválida');
      }
    }
  );

   final validateOrgEmissor = StreamTransformer<String, String>.fromHandlers(
    handleData: (orgEmissor, sink){
      if(orgEmissor.isEmpty){
        sink.add('campo não pode faicar vazio');
      }else {
        sink.add(orgEmissor);
      }
    }
  );


   final validateCpf = StreamTransformer<String, String>.fromHandlers(
    handleData: (cpf, sink){
      if(cpf.isEmpty){
        sink.add('campo não pode faicar vazio');
      }else {
        sink.add(cpf);
      }
    }
  );

   final validateTitulo = StreamTransformer<String, String>.fromHandlers(
    handleData: (titulo, sink){
      if(titulo.isEmpty){
        sink.add('campo não pode faicar vazio');
      }else {
        sink.add(titulo);
      }
    }

   );

 

    final validateNomePai = StreamTransformer<String, String>.fromHandlers(
    handleData: (nomePai, sink){
      if(nomePai.isEmpty){
        sink.add('campo não pode faicar vazio');
      }else {
        sink.add(nomePai);
      }
    }
  );

    final validateNomeMae = StreamTransformer<String, String>.fromHandlers(
    handleData: (nomeMae, sink){
      if(nomeMae.isEmpty){
        sink.add('campo não pode faicar vazio');
      }else {
        sink.add(nomeMae);
      }
    }
  );
  

   final validateNatural = StreamTransformer<String, String>.fromHandlers(
    handleData: (naturalidade, sink){
      if(naturalidade.isEmpty){
        sink.add('campo não pode faicar vazio');
      }else {
        sink.add(naturalidade);
      }
    }
  );


    final validateNacionalidade = StreamTransformer<String, String>.fromHandlers(
    handleData: (nacionalidade, sink){
      if(nacionalidade.isEmpty){
        sink.add('campo não pode faicar vazio');
      }else {
        sink.add(nacionalidade);
      }
    }
  );


}