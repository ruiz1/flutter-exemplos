import 'package:flutter_web/cupertino.dart';
import 'package:flutter_web/material.dart';
import 'package:flutter_web/widgets.dart';
import 'package:web_pv/view/PageScrollView/pageScroll.dart';
import 'package:web_pv/view/largeScreen/first_input_screen.dart';
import 'package:web_pv/view/largeScreen/second_input_screen.dart';


class LargeScreen extends StatelessWidget {
  String nomeCompleto;
  bool _nomeValidate = true;

  PageScroll pageScroll = new PageScroll();
 

  @override
  Widget build(BuildContext context) {
    pageScroll.setPages(FirstInputScreen());
    pageScroll.setPages(SecondInputScreen());
    // TODO: implement build
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Form(
            child: Row(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Container(
                    color: Color(0xff014029),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Container(),
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            margin: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * .09,
                horizontal: MediaQuery.of(context).size.width * .03),
            child: Material(
              elevation: 20.0,
              color: Colors.white,
              shadowColor: Colors.black,
              child: Container(
                height: MediaQuery.of(context).size.height * .5,
                width: MediaQuery.of(context).size.width * .5,
                decoration: BoxDecoration(),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                          height: double.infinity,
                          color: Color(0xff014029),
                          child: Stack(
                            // Logo here
                            children: <Widget>[

                            ],
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: pageScroll,
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column header_left_logo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Image(
          image: AssetImage('logomarca_footer.png'),
          height: 150,
          width: 150,
        ),
        Center(
          child: Text(
            "Ficha de filiação \n  Partido Verde",
            style: TextStyle(color: Colors.white, fontSize: 50),
          ),
        )
      ],
    );
  }
}
