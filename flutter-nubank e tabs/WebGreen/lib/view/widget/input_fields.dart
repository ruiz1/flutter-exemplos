import 'package:flutter_web/material.dart';
import 'package:web_pv/bloc/HomeBloc.dart';
import 'package:web_pv/bloc/mixin/flutter_masked_text.dart.dart';


class MyWidgetsInputs {
  var controller = new MaskedTextController(mask: '000.000.000-00');
  Widget fullName(ValueBloc bloc) {
    return StreamBuilder(
        stream: bloc.fullName,
        builder: (context, snapshot) {
          return TextField(
            onChanged: bloc.fullNameUpdate,
            decoration: InputDecoration(
                labelText: "Nome completo", errorText: snapshot.error),
          );
        });
  }

  Widget dataNasc(ValueBloc bloc) {
    var dataController =  MaskedTextController(mask: '00/00/0000');

    return StreamBuilder(
        stream: bloc.dataNasc,
        builder: (context, snapshot) {
          return Flexible(
            flex: 1,
            child: Container(
              child: TextField(
                controller: dataController,
                onChanged: (text){
                  bloc.dataNascUpdate.add(text);
                },
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Data de Nascimento",
                  errorText: snapshot.error,
                ),
              ),
            ),
          );
        });
  }

  Widget identification(ValueBloc bloc) {
    return StreamBuilder(
        stream: bloc.identificacao,
        builder: (context, snapshot) {
          return Flexible(
            flex: 1,
            child: Container(
              child: TextField(
                onChanged: bloc.identUpdate,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Identidade",
                  errorText: snapshot.error,
                ),
              ),
            ),
          );
        });
  }

  Widget orgaoEmissor(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Óg Emissor"),
            ),
          ),
        ),
      );
    });
  }

  Widget cpfField(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Cpf"),
            ),
          ),
        ),
      );
    });
  }

  Widget tituloEleitor(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Titulo de Eleitor"),
            ),
          ),
        ),
      );
    });
  }

  Widget zonaEleitoral(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Zona"),
            ),
          ),
        ),
      );
    });
  }

  Widget secaoEleitoral(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Seção"),
            ),
          ),
        ),
      );
    });
  }

  Widget naturalidade(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Naturalidade"),
            ),
          ),
        ),
      );
    });
  }

  Widget nacionalidade(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Nacionalidade"),
            ),
          ),
        ),
      );
    });
  }

  Widget nomePai(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Nome do Pai"),
            ),
          ),
        ),
      );
    });
  }

  Widget nomeMae(ValueBloc bloc) {
    return StreamBuilder(builder: (context, snapshot) {
      return Flexible(
        flex: 1,
        child: Container(
          child: Container(
            child: TextField(
              decoration: InputDecoration(hintText: "Nome da Mãe"),
            ),
          ),
        ),
      );
    });
  }

}


 class MaskedFormField{
  @override
  // TODO: implement maskedData
  static get maskedData => new MaskedTextController(mask: '00/00/0000');
  
} 
