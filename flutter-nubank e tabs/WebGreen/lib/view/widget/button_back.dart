import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_web/material.dart';
import 'package:web_pv/bloc/BlocForm.dart';

class ButtonBack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BlocForm blocForm = BlocProvider.getBloc<BlocForm>();

    return MaterialButton(
        onPressed: () {
          blocForm.nextPageAdd.add(0);
        },
        child: Container(
            
            child: Row(
              children: <Widget>[
                 Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black38,
                ),
                Text(
                  'VOLTAR',
                  style: TextStyle(color: Colors.black38, fontSize: 15),
                ),
               
              ],
            )));
  }
}
