import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_web/material.dart';
import 'package:web_pv/bloc/HomeBloc.dart';

class ButtonSubmmit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ValueBloc bloc = BlocProvider.getBloc<ValueBloc>();
      return StreamBuilder<bool>(
        stream: bloc.submitCheck,
        builder: (context, snapshot) {
          return MaterialButton(
            onPressed: () {
               snapshot.hasData ? bloc.submitButtonPressed() : null;
            },
            child: Container(
                   
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'ENVIAR',
                      style: TextStyle(color: Colors.black38, fontSize: 20),
                    ),
            
                  ],
                )));
        }
      );

  }
}
