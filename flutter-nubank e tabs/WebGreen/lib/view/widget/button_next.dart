import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_web/material.dart';
import 'package:web_pv/bloc/BlocForm.dart';

class ButtonNext extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BlocForm blocForm = BlocProvider.getBloc<BlocForm>();

    return MaterialButton(
        onPressed: () {
          blocForm.nextPageAdd.add(1);
        },
        child: Container(
           
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  'NEXT',
                  style: TextStyle(color: Colors.black38, fontSize: 15),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.black38,
                )
              ],
            )));
  }
}
