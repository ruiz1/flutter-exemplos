import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_web/material.dart';
import 'package:web_pv/bloc/BlocForm.dart';
import 'package:web_pv/bloc/HomeBloc.dart';

class PageScroll extends StatelessWidget {
  List _list_pages = [];
  int _indexPage = 0;

  @override
  Widget build(BuildContext context) {
    final BlocForm blocForm = BlocProvider.getBloc<BlocForm>();
    
    return StreamBuilder<int>(
      stream: blocForm.nextPageOut,
      builder: (context, snapshot) {
        return PageView.builder(
          itemBuilder: (context, position){
            if(snapshot.hasData){
               _indexPage = snapshot.data;
            }
            return this._list_pages[_indexPage];
          },
          itemCount: this._list_pages.length,
        );
      }
    );
  }

  setPages(Widget pages){
    _list_pages.add(pages);
  }

}