import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_web/material.dart';
import 'package:web_pv/bloc/HomeBloc.dart';
import 'package:web_pv/view/widget/button_back.dart';
import 'package:web_pv/view/widget/button_submmit.dart';
import 'package:web_pv/view/widget/input_fields.dart';

class SecondInputScreen extends StatelessWidget {
  MyWidgetsInputs myWidget = MyWidgetsInputs();

  @override
  Widget build(BuildContext context) {
     final ValueBloc bloc = BlocProvider.getBloc<ValueBloc>();
   return  Container(
     color: Colors.grey[200],
     child: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Padding(
          padding: EdgeInsets.only(top: 60),
          child: StreamBuilder(
              initialData: 0,
              builder: (context, snapshot) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    myWidget.fullName(bloc),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        myWidget.nomePai(bloc),
                        SizedBox(
                          width: 10,
                        ),
                        myWidget.nomeMae(bloc),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        myWidget.dataNasc(bloc),
                        SizedBox(
                          width: 10,
                        ),
                        myWidget.identification(bloc),
                        SizedBox(
                          width: 10,
                        ),
                        myWidget.orgaoEmissor(bloc),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        myWidget.cpfField(bloc),
                        SizedBox(
                          width: 10,
                        ),
                        myWidget.tituloEleitor(bloc),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        myWidget.zonaEleitoral(bloc),
                        SizedBox(
                          width: 10,
                        ),
                        myWidget.secaoEleitoral(bloc),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        myWidget.naturalidade(bloc),
                        SizedBox(
                          width: 10,
                        ),
                        myWidget.nacionalidade(bloc),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ButtonBack(),
                        ButtonSubmmit(),
                      ],
                    )
                  ],
                );
              }),
        ),
      ),
   );
  }
}