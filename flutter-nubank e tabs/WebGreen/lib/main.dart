import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_web/material.dart';
import 'package:web_pv/bloc/BlocForm.dart';
import 'package:web_pv/bloc/HomeBloc.dart';
import 'package:web_pv/cadastro.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => ValueBloc()),
        Bloc((i) => BlocForm()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        home: Cadastro(),
      ),
    );
  }
}
