import 'package:flutter/material.dart';
import 'package:nubank_speed/screens/splashscreen.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:nubank_speed/screens/tabs_bloc.dart';
import 'package:nubank_speed/shared/repositories/custom_dio.dart';
import 'package:nubank_speed/shared/repositories/general_api.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var app = MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: SplashScreen(),
    );


    return BlocProvider(
      child: app,
      dependencies: [
        Dependency((i) => CustomDio()),
        Dependency((i) => GeneralApi(i.get<CustomDio>().getClient())),
      ],
      blocs: [
        Bloc((i) => TabsBloc(i.get<GeneralApi>())),
      ],
    );

    
  }
}
