import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:nubank_speed/shared/repositories/general_api.dart';
import 'package:rxdart/rxdart.dart';

class TabsBloc extends BlocBase 
{
  final GeneralApi api;
  // ingeta a api no contrutor da classe junto com dio
  TabsBloc(this.api);
  final BehaviorSubject _listController = BehaviorSubject.seeded(true);

  Sink get listInput => _listController.sink;
  Observable<List> get listOut => _listController.stream.asyncMap(
    (v)=>api.getPost());

  @override
  void dispose() {
    _listController.close();
    super.dispose();
  }


  
  
}