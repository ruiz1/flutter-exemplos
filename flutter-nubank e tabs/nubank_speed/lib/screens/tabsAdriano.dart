import 'package:flutter/material.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:nubank_speed/screens/dashcreen.dart';
import 'package:nubank_speed/screens/tabs_bloc.dart';

class TabsAdrianoPage extends StatefulWidget {
  @override
  _TabsAdrianoPageState createState() => _TabsAdrianoPageState();
}

class _TabsAdrianoPageState extends State<TabsAdrianoPage>
    with SingleTickerProviderStateMixin {
  //  aqui é feito a injesao de dependencia
  final bloc = BlocProvider.getBloc<TabsBloc>();
  TabController _tabController;
  int _currentIndex = 0;
  VoidCallback onChanged;

  var retApi;

  final List<Tab> myTabs = <Tab>[
    Tab(text: 'Adriano'),
    Tab(text: 'Lindo'),
    Tab(text: 'De mais'),
    Tab(text: 'Dev'),
    Tab(text: 'Melhor'),
    Tab(text: 'De'),
    Tab(text: 'Todos'),
    Tab(text: 'Muito'),
    Tab(text: 'BOM'),
    Tab(text: 'BOM'),
    Tab(text: 'BOMMM'),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);

    onChanged = () {
      setState(() {
        _currentIndex = this._tabController.index;
        retApi = _tabController.index.toString() + ' teste lalaa';
        print(_tabController.index);
      });
    };
    _tabController.addListener(onChanged);
  }

  @override
  void dispose() {
    // Dispose of the Tab Controller
    _tabController.removeListener(onChanged);
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('7cliques'),
        bottom: TabBar(
          controller: _tabController,
          isScrollable: true,
          unselectedLabelColor: Colors.white.withOpacity(0.3),
          indicatorColor: Colors.white,
          tabs: myTabs,
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: myTabs.map((Tab tab) {
          return StreamBuilder<List>(
            stream: bloc.listOut,
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(
                  child: CircularProgressIndicator(),
                );
              var posts = snapshot.data;
              // print(posts);
              return ListView.separated(
                itemCount: posts.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    onTap: (){
    Navigator.push(context, MaterialPageRoute(builder: (context) => DashScreen()));

                    },
                    title: Text(posts[index]['name']),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider();
                },
              );
            },
          );
          // return Center(
          //   //child: Text(tab.text)
          //   child:  Text(retApi ?? '')
          // );
        }).toList(),
      ),
    );
  }
}
