import 'package:bloc_dio/src/screens/home/home_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';


class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  //  aqui é feito a injesao de dependencia
  final bloc = BlocProvider.getBloc<HomeBloc>();

  // @override
  // void initState() {
    
  //   bloc = HomeBloc(GeneralApi());
  //   super.initState();
  // }

  // @override
  // void dispose() {
  //   bloc.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        title: Text('7clicks'),
      ),
      body: StreamBuilder<List>(
        stream: bloc.listOut,
        builder: (context, snapshot){
          if(!snapshot.hasData) return Center(child: CircularProgressIndicator(),);
          var posts = snapshot.data;
          // print(posts);
          return ListView.separated(
            itemCount: posts.length,
            itemBuilder: (BuildContext context, int index){
              return ListTile(title: Text(posts[index]['title']),);
            },separatorBuilder: (BuildContext context, int index) {
              return Divider();
            },
          );

        },
      ),
    );
  }
}