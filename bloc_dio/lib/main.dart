import 'package:bloc_dio/src/screens/home/home_bloc.dart';
import 'package:bloc_dio/src/screens/home/home_screen.dart';
import 'package:bloc_dio/src/shared/repositories/custom_dio.dart';
import 'package:bloc_dio/src/shared/repositories/general_api.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var app = MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(title: 'Flutter Demo Home Page'),
    );

    return BlocProvider(
      child: app,
      dependencies: [
        Dependency((i) => CustomDio()),
        Dependency((i) => GeneralApi(i.get<CustomDio>().getClient())),
      ],
      blocs: [
        Bloc((i) => HomeBloc(i.get<GeneralApi>())),
      ],
    );
  }
}
