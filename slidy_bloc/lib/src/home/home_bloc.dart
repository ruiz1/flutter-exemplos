import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cliques_bloc/src/home/home_module.dart';
import 'package:cliques_bloc/src/home/repositories/post_repository.dart';

class HomeBloc extends BlocBase {
  
  var _postRepository = HomeModule.to.getDependency<PostRepository>();
  
  //dispose will be called automatically by closing its streams
  @override
  void dispose() {
    super.dispose();
  }
}
