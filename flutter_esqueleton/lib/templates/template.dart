import 'package:flutter/material.dart';
import 'package:quefazer/templates/menuDrawer.dart';


class Template extends StatefulWidget {
  final String title;
  final bool menubar;
  final bool drawer;
  final bool menuBarImage;
  final bool logo;
  final Function builder;


  Template(
      {Key key,
      this.title,
      this.menubar,
      this.menuBarImage = false,
      this.logo = false,
      this.drawer = false,
      this.builder,
    
      ListView body})
      : super(key: key);

  @override
  _TemplateState createState() => _TemplateState();
}

class _TemplateState extends State<Template> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child:  Stack(
          children: <Widget>[
             Scaffold(
                drawer: DrawerUser(),
                appBar:  AppBar(
                  centerTitle: true,
                  title: Text(widget.title),
                  backgroundColor: Colors.red,
                  elevation: 0.0,
                ),
                backgroundColor: Colors.red[50],
                body:  Container(
                  decoration: BoxDecoration(color: Colors.white),
                  child: widget.builder(),
                ))
          ],
        ),
      );
  }
}
