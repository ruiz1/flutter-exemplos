import 'package:flutter/material.dart';
import 'package:quefazer/screens/home/home.dart';

//Menu Drawer Gerador
class DrawerUser extends StatefulWidget {
  @override
  _DrawerUserState createState() => _DrawerUserState();
}

class _DrawerUserState extends State<DrawerUser> {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new Container(
        decoration: BoxDecoration(color: Colors.white),
        child: ListView(
          children: <Widget>[
            new DrawerHeader(
              decoration: BoxDecoration(color: Colors.white),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/avatar.png'),
                              fit: BoxFit.cover)),
                      child: Text(''),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      flex: 7,
                      child: Padding(
                        padding: EdgeInsets.only(left: 10, top: 40),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                new Text(
                                  'Flávia Pelissari Sales',
                                  style: TextStyle(
                                      color: Color.fromRGBO(30, 30, 30, 1),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                new Text(
                                  'contato@email.com.br',
                                  style: TextStyle(
                                      color: Color.fromRGBO(30, 30, 30, 1),
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: <Widget>[
                                new Text(
                                  'Super consciente',
                                  style: TextStyle(
                                      color: Color.fromRGBO(0, 179, 63, 1),
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal),
                                )
                              ],
                            )
                          ],
                        ),
                      ))
                ],
              ),
            ),
            new ListTile(
              title: new Text(
                'Home',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(30, 30, 30, 1)),
              ),
              trailing: Icon(Icons.chevron_right,
                  color: Color.fromRGBO(30, 30, 30, 1)),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(),
                  ),
                );
              },
            ),
            new Divider(
              color: Colors.black45,
            ),
            new ListTile(
              title: new Text('Armário',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(30, 30, 30, 1))),
              trailing: Icon(Icons.chevron_right,
                  color: Color.fromRGBO(30, 30, 30, 1)),
            ),
            new Divider(color: Colors.black45),
            new ListTile(
              title: new Text('Materias coletados',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(30, 30, 30, 1))),
              trailing: Icon(Icons.chevron_right,
                  color: Color.fromRGBO(30, 30, 30, 1)),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(),
                  ),
                );
              },
            ),
            new Divider(color: Colors.black45),
            new ListTile(
              title: new Text('Transferência',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(30, 30, 30, 1))),
              trailing: Icon(Icons.chevron_right,
                  color: Color.fromRGBO(30, 30, 30, 1)),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(),
                  ),
                );
              },
            ),
            new Divider(color: Colors.black45),
            new ListTile(
              title: new Text('Entidades filantrópicas',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(30, 30, 30, 1))),
              trailing: Icon(Icons.chevron_right,
                  color: Color.fromRGBO(30, 30, 30, 1)),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(),
                  ),
                );
              },
            ),
            new Divider(color: Colors.black45),
            new ListTile(
              title: new Text('Certificados',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(30, 30, 30, 1))),
              trailing: Icon(Icons.chevron_right,
                  color: Color.fromRGBO(30, 30, 30, 1)),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(),
                  ),
                );
              },
            ),
            new Divider(color: Colors.black45),
            new ListTile(
              title: new Text('Perfil',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(30, 30, 30, 1))),
              trailing: Icon(Icons.chevron_right,
                  color: Color.fromRGBO(30, 30, 30, 1)),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(),
                  ),
                );
              },
            ),
            new Padding(
              padding: EdgeInsets.only(top: 60, bottom: 20),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomeScreen(),
                    ),
                  );
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Icon(Icons.exit_to_app,
                          color: Color.fromRGBO(30, 30, 30, 1)),
                    ),
                    Expanded(
                      flex: 5,
                      child: new Text('Sair',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(30, 30, 30, 1))),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}



