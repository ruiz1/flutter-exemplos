import 'package:quefazer/screens/home/home_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:quefazer/templates/template.dart';

import 'home_bloc.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  //  aqui é feito a injesao de dependencia
  final bloc = BlocProvider.getBloc<HomeBloc>();


  @override
  Widget build(BuildContext context) {
  return  Template(
        title: '7clicks home',
        menubar: false,
        drawer: false,
        builder: () {
          return myPosts(context);
        });
  }

  Widget myPosts(context){
   return StreamBuilder<List>(
            stream: bloc.listOut,
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(
                  child: CircularProgressIndicator(),
                );
              var posts = snapshot.data;
              print(posts);
              return ListView.separated(
                itemCount: posts.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(posts[index]['title']),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider();
                },
              );
            },
          );
  }
}
