import 'package:quefazer/screens/home/home_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:quefazer/screens/login/login._bloc.dart';


class LoginScreen extends StatefulWidget {
  LoginScreen({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  //  aqui é feito a injesao de dependencia
  final bloc = BlocProvider.getBloc<LoginBloc>();

  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: StreamBuilder<List>(
        stream: bloc.listOut,
        builder: (context, snapshot){
          if(!snapshot.hasData) return Center(child: CircularProgressIndicator(),);
          var posts = snapshot.data;
          // print(posts);
          return ListView.separated(
            itemCount: posts.length,
            itemBuilder: (BuildContext context, int index){
              return ListTile(title: Text(posts[index]['name']),);
            },separatorBuilder: (BuildContext context, int index) {
              return Divider();
            },
          );
        },
      ),
    );
  }
}