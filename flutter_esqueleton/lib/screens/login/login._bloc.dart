import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:quefazer/shared/repositories/general_api.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends BlocBase 
{
  final GeneralApi api;
  
  LoginBloc(this.api);

  final BehaviorSubject _listController = BehaviorSubject.seeded(true);

  Sink get listInput => _listController.sink;
  Observable<List> get listOut => _listController.stream.asyncMap(
    (v)=>api.getUsers());

  @override
  void dispose() {
    _listController.close();
    super.dispose();
  }


  
  
}