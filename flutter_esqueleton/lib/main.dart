import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:quefazer/screens/home/home.dart';
import 'package:quefazer/screens/home/home_bloc.dart';
import 'package:quefazer/screens/login/login._bloc.dart';
import 'package:quefazer/screens/login/login.dart';
import 'package:quefazer/shared/repositories/custom_dio.dart';
import 'package:quefazer/shared/repositories/general_api.dart';
import 'package:quefazer/templates/template.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var app = MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false, 
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: HomeScreen(title: 'Que Fazer')
      );
      
    

    return BlocProvider(
      child: app,
      dependencies: [
        Dependency((i) => CustomDio()),
        Dependency((i) => GeneralApi(i.get<CustomDio>().getClient())),
      ], blocs: [
        Bloc((i) => HomeBloc(i.get<GeneralApi>())),
        Bloc((i) => LoginBloc(i.get<GeneralApi>())),

      ],
    );
  }

}
