import 'package:flutter/cupertino.dart';
import 'package:providers/models/hero_model.dart';

class HeroesController extends ChangeNotifier {
  List<HeroModel> heroes = [
    HeroModel(name: "Thor"),
    HeroModel(name: "Iron man", isFavorte: true),
    HeroModel(name: "BatMan"),
    HeroModel(name: "Adriano", isFavorte: true),
    HeroModel(name: "José"),
    HeroModel(name: "Mari")

  ];

  checkFavorite(HeroModel model){
    model.isFavorte = !model.isFavorte;
    notifyListeners();
  }
  
}