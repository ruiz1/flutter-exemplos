import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providers/controllers/heroes_controller.dart';
import 'package:providers/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers:[
        ChangeNotifierProvider<HeroesController>.value(value: HeroesController(),)
      ], 
      child:MaterialApp(
      title: 'Provider',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    )
    );
    
  }
}

 