import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providers/models/hero_model.dart';
import 'package:providers/controllers/heroes_controller.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  _buildList(HeroesController heroController){
    return ListView.builder(
      itemCount: heroController.heroes.length,
      itemBuilder: (context, index){
        return _buildItems(heroController.heroes[index], heroController);
      },

    );
  }


  _buildItems(HeroModel model, HeroesController heros){
    // aqui se quiser chamar o objeto heroesController direto do provider sem passar por paramentro
    // porem preferi deixar pasando por parametro mesmo.
    HeroesController heroesController = Provider.of<HeroesController>(context);
    return ListTile(
      onTap: (){
        heros.checkFavorite(model);
      },
      title: Text(model.name),
      trailing:
      model.isFavorte ? 
       Icon(Icons.star, color: Colors.orange,) :
       Icon(Icons.star_border),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Providers'),
          leading: Consumer<HeroesController>(
            builder: (context, heroesController, widget) {
              return Center(child: Text("${heroesController.heroes.where((i)=> i.isFavorte ).length}",
              style: TextStyle(fontSize: 18),
              ));
            },
          ),
        ),
        body: Consumer<HeroesController>(
          builder: (context, heroesController, widget){
            return _buildList(heroesController);
          },
        ),
    );
  }
}