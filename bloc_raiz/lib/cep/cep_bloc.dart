import 'dart:async';

import 'package:bloc_raiz/cep/cep_model.dart';
import 'package:dio/dio.dart';

class CepPageBloc {
  final StreamController<String> _streamcontroller =
      StreamController<String>.broadcast();
  Sink<String> get input => _streamcontroller.sink;
  Stream<CepModel> get output => _streamcontroller.stream
      .where((cep) => cep.length > 7)
      .asyncMap((cep) => _searchCep(cep));

  String url(String cep) => "https://viacep.com.br/ws/$cep/json/";

  Future<CepModel> _searchCep(String cep) async {
    try {

      print(url(cep));
      Response response = await Dio().get(url(cep));
      print(response.statusCode);
      if(response.statusCode != 200){
        return null;

      }
      return CepModel.fromJson(response.data);

    } on DioError catch (e) {

      print(e);
      
    }
  }
}
