import 'package:bloc_raiz/cep/cep_bloc.dart';
import 'package:flutter/material.dart';

import 'cep_model.dart';

class CepPage extends StatefulWidget {
  CepPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CepPageState createState() => _CepPageState();
}

class _CepPageState extends State<CepPage> {
  CepPageBloc bloc = CepPageBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cep'),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              TextField(
                onChanged: (value){
                  bloc.input.add(value);
                },
                decoration: InputDecoration(
                  hintText: 'Digite seu cep:',
                  border: OutlineInputBorder(),
                ),
              ),

              SizedBox(height: 0,),
              Center(child: StreamBuilder<CepModel>(
                stream: bloc.output,
                initialData: CepModel(bairro: 'Sem bairro'),

                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.active &&
              snapshot.hasError ) {
                   return Text("Erro na pesquisa",
                    style: TextStyle(color: Colors.red),

                    );
                  }

                  CepModel endereco = snapshot.data;
                  print(snapshot.data);
                  return Text("Bairro ${endereco.bairro}");
                }

              )),

              // Text(
              //   'You have pushed the button this many times:',
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
